-- ----------------------------
-- 升级数据结构
-- ----------------------------
-- v1.8.1 -> v1.8.2
ALTER TABLE `ai_system_member` ADD COLUMN `auth` INT(11) NULL DEFAULT 0 AFTER `ticket`;
ALTER TABLE `ai_system_miniapp` ADD COLUMN `is_diyapp` TINYINT(1) NULL DEFAULT 0 AFTER `is_lock`;