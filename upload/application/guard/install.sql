/*
Navicat MySQL Data Transfer

Source Server         : 本地开发
Source Server Version : 50721
Source Host           : localhost:3306
Source Database       : wechat

Target Server Type    : MYSQL
Target Server Version : 50721
File Encoding         : 65001

Date: 2020-06-13 18:37:52
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for ai_guard
-- ----------------------------
DROP TABLE IF EXISTS `ai_guard`;
CREATE TABLE `ai_guard` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `member_miniapp_id` int(11) DEFAULT NULL,
  `uid` int(11) DEFAULT NULL,
  `company` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `telphone` varchar(255) DEFAULT NULL,
  `create_time` int(11) DEFAULT NULL,
  `update_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for ai_guard_history
-- ----------------------------
DROP TABLE IF EXISTS `ai_guard_history`;
CREATE TABLE `ai_guard_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `member_miniapp_id` int(11) DEFAULT NULL,
  `gid` int(11) DEFAULT NULL,
  `uid` int(11) DEFAULT NULL,
  `car_num` varchar(100) DEFAULT NULL,
  `is_danger` tinyint(4) DEFAULT NULL,
  `temperature` varchar(100) DEFAULT NULL,
  `why` varchar(255) DEFAULT NULL,
  `pass_out` varchar(10) DEFAULT '',
  `update_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for ai_guard_user
-- ----------------------------
DROP TABLE IF EXISTS `ai_guard_user`;
CREATE TABLE `ai_guard_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `member_miniapp_id` int(11) DEFAULT NULL,
  `uid` int(11) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `phone` varchar(100) DEFAULT NULL,
  `idcard` varchar(100) DEFAULT NULL,
  `car_num` varchar(100) DEFAULT NULL,
  `pass_out` varchar(100) DEFAULT NULL,
  `create_time` int(11) DEFAULT NULL,
  `update_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
SET FOREIGN_KEY_CHECKS=1;
