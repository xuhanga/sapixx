/*
Navicat MySQL Data Transfer

Source Server         : 本地开发
Source Server Version : 50721
Source Host           : localhost:3306
Source Database       : wechat

Target Server Type    : MYSQL
Target Server Version : 50721
File Encoding         : 65001

Date: 2020-06-13 17:48:09
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for ai_green_adwords
-- ----------------------------
DROP TABLE IF EXISTS `ai_green_adwords`;
CREATE TABLE `ai_green_adwords` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `member_miniapp_id` int(11) DEFAULT '0',
  `open_type` varchar(50) DEFAULT NULL,
  `group` varchar(100) DEFAULT NULL,
  `title` varchar(50) DEFAULT NULL,
  `link` varchar(200) DEFAULT NULL,
  `picture` varchar(255) DEFAULT NULL,
  `sort` int(11) DEFAULT '0',
  `update_time` int(11) DEFAULT NULL,
  `create_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for ai_green_alarm
-- ----------------------------
DROP TABLE IF EXISTS `ai_green_alarm`;
CREATE TABLE `ai_green_alarm` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(255) DEFAULT NULL,
  `content` varchar(255) DEFAULT NULL,
  `state` tinyint(1) DEFAULT '0',
  `create_time` int(11) DEFAULT NULL,
  `relieve_time` int(11) DEFAULT NULL,
  `member_miniapp_id` int(11) DEFAULT NULL,
  `operate_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `CODEID` (`device_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for ai_green_bank_cash
-- ----------------------------
DROP TABLE IF EXISTS `ai_green_bank_cash`;
CREATE TABLE `ai_green_bank_cash` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `member_miniapp_id` int(11) DEFAULT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `money` decimal(10,2) DEFAULT '0.00' COMMENT '申请金额',
  `realmoney` decimal(10,2) DEFAULT '0.00' COMMENT '实际到账',
  `update_time` int(11) DEFAULT NULL,
  `state` tinyint(1) DEFAULT '0' COMMENT '0新申请1通过-1不通过',
  `audit_time` int(11) DEFAULT NULL,
  `trade_no` varchar(100) DEFAULT NULL,
  `msg` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='提现申请列表';

-- ----------------------------
-- Table structure for ai_green_category
-- ----------------------------
DROP TABLE IF EXISTS `ai_green_category`;
CREATE TABLE `ai_green_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `member_miniapp_id` int(11) DEFAULT NULL,
  `parent_id` int(11) DEFAULT '0',
  `root_id` int(11) DEFAULT NULL,
  `title` varchar(50) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `picture` varchar(100) DEFAULT NULL,
  `price` decimal(10,2) DEFAULT NULL,
  `sort` int(11) DEFAULT '0',
  `update_time` int(11) DEFAULT NULL,
  `create_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ID` (`id`) USING BTREE,
  KEY `PARENT_ID` (`parent_id`,`root_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='分类信息栏目';

-- ----------------------------
-- Table structure for ai_green_config
-- ----------------------------
DROP TABLE IF EXISTS `ai_green_config`;
CREATE TABLE `ai_green_config` (
  `member_miniapp_id` int(11) NOT NULL DEFAULT '0',
  `config` text,
  `service_telephone` varchar(100) DEFAULT NULL,
  `help` text,
  `is_wechat_touser` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`member_miniapp_id`),
  UNIQUE KEY `APPID` (`member_miniapp_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for ai_green_device
-- ----------------------------
DROP TABLE IF EXISTS `ai_green_device`;
CREATE TABLE `ai_green_device` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(255) DEFAULT NULL,
  `operate_id` int(11) DEFAULT NULL,
  `member_miniapp_id` int(11) DEFAULT NULL,
  `manage_uid` int(11) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `state` tinyint(1) DEFAULT '0',
  `type` tinyint(1) DEFAULT '0',
  `longitude` decimal(10,7) DEFAULT NULL COMMENT '地图坐标',
  `latitude` decimal(10,7) DEFAULT NULL,
  `ip` varchar(255) DEFAULT NULL,
  `port` varchar(255) DEFAULT NULL,
  `create_time` int(11) DEFAULT NULL,
  `update_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `CODEID` (`device_id`),
  KEY `APPID` (`member_miniapp_id`),
  KEY `CITYID` (`operate_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for ai_green_device_state
-- ----------------------------
DROP TABLE IF EXISTS `ai_green_device_state`;
CREATE TABLE `ai_green_device_state` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(255) DEFAULT NULL,
  `uid` int(11) DEFAULT NULL,
  `state` tinyint(1) DEFAULT '0',
  `type` tinyint(1) DEFAULT NULL,
  `ip` varchar(255) DEFAULT NULL,
  `port` varchar(255) DEFAULT NULL,
  `user_content` varchar(255) DEFAULT NULL,
  `device_content` varchar(255) DEFAULT NULL,
  `end_content` varchar(255) DEFAULT NULL,
  `create_time` int(11) DEFAULT NULL,
  `update_time` int(11) DEFAULT NULL,
  `member_miniapp_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `CODEID` (`device_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for ai_green_job
-- ----------------------------
DROP TABLE IF EXISTS `ai_green_job`;
CREATE TABLE `ai_green_job` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `member_miniapp_id` int(11) DEFAULT NULL,
  `uid` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `city` varchar(50) DEFAULT NULL,
  `occupation` varchar(50) DEFAULT NULL,
  `card` varchar(50) DEFAULT NULL,
  `front` varchar(255) DEFAULT NULL,
  `back` varchar(255) DEFAULT '0',
  `create_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for ai_green_member
-- ----------------------------
DROP TABLE IF EXISTS `ai_green_member`;
CREATE TABLE `ai_green_member` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `member_miniapp_id` int(11) DEFAULT NULL,
  `member_id` int(11) DEFAULT NULL,
  `operate_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for ai_green_news
-- ----------------------------
DROP TABLE IF EXISTS `ai_green_news`;
CREATE TABLE `ai_green_news` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `member_miniapp_id` int(11) DEFAULT NULL,
  `cate_id` int(11) DEFAULT NULL,
  `cate_name` varchar(255) DEFAULT NULL,
  `types` tinyint(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `content` longtext,
  `desc` varchar(255) DEFAULT NULL,
  `img` varchar(255) DEFAULT NULL,
  `views` bigint(20) DEFAULT '0',
  `update_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for ai_green_news_cate
-- ----------------------------
DROP TABLE IF EXISTS `ai_green_news_cate`;
CREATE TABLE `ai_green_news_cate` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `member_miniapp_id` int(11) DEFAULT NULL,
  `title` varchar(50) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `picture` varchar(100) DEFAULT NULL,
  `sort` int(11) DEFAULT '0',
  `update_time` int(11) DEFAULT NULL,
  `create_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ID` (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='新闻分类';

-- ----------------------------
-- Table structure for ai_green_operate
-- ----------------------------
DROP TABLE IF EXISTS `ai_green_operate`;
CREATE TABLE `ai_green_operate` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `member_miniapp_id` int(11) DEFAULT NULL,
  `uid` bigint(20) DEFAULT NULL,
  `operate_name` varchar(100) DEFAULT NULL,
  `address` varchar(100) DEFAULT NULL,
  `tel` varchar(32) DEFAULT NULL,
  `is_lock` tinyint(1) DEFAULT '0',
  `company` varchar(255) DEFAULT NULL,
  `create_time` int(11) DEFAULT NULL,
  `longitude` decimal(10,7) DEFAULT NULL COMMENT '地图坐标',
  `latitude` decimal(10,7) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for ai_green_order
-- ----------------------------
DROP TABLE IF EXISTS `ai_green_order`;
CREATE TABLE `ai_green_order` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `member_miniapp_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `shop_id` int(11) DEFAULT NULL,
  `order_no` varchar(50) DEFAULT NULL,
  `is_del` tinyint(1) DEFAULT '0',
  `paid_at` tinyint(1) DEFAULT '0',
  `amount` decimal(10,2) DEFAULT '0.00' COMMENT '单商品价格总额',
  `points` bigint(11) DEFAULT NULL,
  `create_time` int(11) DEFAULT NULL,
  `paid_time` int(11) DEFAULT NULL,
  `paid_no` varchar(255) DEFAULT NULL,
  `message` varchar(255) DEFAULT NULL,
  `shop_cache` text,
  `express_status` tinyint(1) DEFAULT '0',
  `express_name` varchar(50) DEFAULT NULL,
  `express_phone` varchar(20) DEFAULT NULL,
  `express_address` varchar(255) DEFAULT NULL,
  `express_starttime` int(11) DEFAULT NULL,
  `express_company` varchar(100) DEFAULT NULL COMMENT '快递公司',
  `express_no` varchar(50) DEFAULT NULL COMMENT '快递单号',
  PRIMARY KEY (`id`),
  UNIQUE KEY `ORDER_NO` (`order_no`) USING BTREE,
  KEY `IS_DEL` (`is_del`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for ai_green_recruit
-- ----------------------------
DROP TABLE IF EXISTS `ai_green_recruit`;
CREATE TABLE `ai_green_recruit` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `member_miniapp_id` int(11) DEFAULT NULL,
  `title` varchar(50) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `state` tinyint(2) DEFAULT NULL,
  `news_id` int(11) DEFAULT NULL,
  `sort` int(11) DEFAULT '0',
  `update_time` int(11) DEFAULT NULL,
  `create_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ID` (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='新闻分类';

-- ----------------------------
-- Table structure for ai_green_retrieve
-- ----------------------------
DROP TABLE IF EXISTS `ai_green_retrieve`;
CREATE TABLE `ai_green_retrieve` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `member_miniapp_id` int(11) DEFAULT NULL,
  `uid` int(11) DEFAULT NULL,
  `realname` varchar(50) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `order_no` varchar(50) DEFAULT NULL,
  `message` varchar(255) DEFAULT NULL,
  `state` tinyint(1) DEFAULT '0',
  `create_time` int(11) DEFAULT NULL,
  `weight` int(11) DEFAULT NULL,
  `longitude` decimal(10,7) DEFAULT NULL COMMENT '地图坐标',
  `latitude` decimal(10,7) DEFAULT NULL,
  `date` int(11) DEFAULT NULL,
  `update_time` int(11) DEFAULT NULL,
  `operate_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ORDER_NO` (`order_no`) USING BTREE,
  KEY `IS_DEL` (`state`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for ai_green_shop
-- ----------------------------
DROP TABLE IF EXISTS `ai_green_shop`;
CREATE TABLE `ai_green_shop` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `member_miniapp_id` int(11) DEFAULT NULL,
  `category_id` bigint(20) DEFAULT NULL,
  `category_path_id` varchar(255) DEFAULT NULL,
  `is_sale` tinyint(1) DEFAULT '0' COMMENT '0:下架、1:上架,',
  `is_del` tinyint(1) DEFAULT '0',
  `name` varchar(255) DEFAULT NULL,
  `note` varchar(255) DEFAULT NULL,
  `points` int(11) DEFAULT '0',
  `imgs` text,
  `img` varchar(255) DEFAULT NULL,
  `content` mediumtext,
  `sort` int(11) DEFAULT NULL,
  `update_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ID` (`id`) USING BTREE,
  KEY `IS_SALE` (`is_sale`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='商品主表';

-- ----------------------------
-- Table structure for ai_green_sign
-- ----------------------------
DROP TABLE IF EXISTS `ai_green_sign`;
CREATE TABLE `ai_green_sign` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` bigint(20) DEFAULT NULL,
  `startime` int(11) DEFAULT NULL,
  `signtime` int(11) DEFAULT NULL,
  `days` int(11) DEFAULT NULL,
  `points` int(11) DEFAULT NULL,
  `member_miniapp_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for ai_green_sign_config
-- ----------------------------
DROP TABLE IF EXISTS `ai_green_sign_config`;
CREATE TABLE `ai_green_sign_config` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `member_miniapp_id` int(11) DEFAULT NULL,
  `config_id` int(11) DEFAULT '1',
  `point` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for ai_green_staff
-- ----------------------------
DROP TABLE IF EXISTS `ai_green_staff`;
CREATE TABLE `ai_green_staff` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `member_miniapp_id` int(11) DEFAULT NULL,
  `uid` int(11) DEFAULT NULL,
  `title` varchar(50) DEFAULT NULL,
  `about` varchar(255) DEFAULT NULL,
  `sort` int(11) DEFAULT '0',
  `operate_id` int(11) DEFAULT NULL,
  `update_time` int(11) DEFAULT NULL,
  `create_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ID` (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='工程师';

-- ----------------------------
-- Table structure for ai_green_user
-- ----------------------------
DROP TABLE IF EXISTS `ai_green_user`;
CREATE TABLE `ai_green_user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `member_miniapp_id` int(11) DEFAULT NULL,
  `uid` int(11) DEFAULT NULL,
  `weight` bigint(20) DEFAULT NULL,
  `points` bigint(20) DEFAULT NULL,
  `update_time` int(11) DEFAULT NULL,
  `create_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `APPID` (`member_miniapp_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for ai_green_user_log
-- ----------------------------
DROP TABLE IF EXISTS `ai_green_user_log`;
CREATE TABLE `ai_green_user_log` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `member_miniapp_id` int(11) DEFAULT NULL,
  `uid` int(11) DEFAULT NULL,
  `weight` decimal(10,0) DEFAULT NULL,
  `device_id` varchar(255) DEFAULT NULL,
  `create_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `APPID` (`member_miniapp_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
SET FOREIGN_KEY_CHECKS=1;
