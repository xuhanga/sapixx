/*
Navicat MySQL Data Transfer

Source Server         : 本地开发
Source Server Version : 50721
Source Host           : localhost:3306
Source Database       : wechat

Target Server Type    : MYSQL
Target Server Version : 50721
File Encoding         : 65001

Date: 2020-03-11 14:40:30
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for ai_popupshop_adwords
-- ----------------------------
CREATE TABLE `ai_popupshop_adwords` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `member_miniapp_id` int(11) DEFAULT '0',
  `group_id` tinyint(1) DEFAULT NULL,
  `open_type` varchar(100) DEFAULT NULL,
  `title` varchar(50) DEFAULT NULL,
  `link` varchar(200) DEFAULT NULL,
  `picture` varchar(255) DEFAULT NULL,
  `sort` int(11) DEFAULT '0',
  `update_time` int(11) DEFAULT NULL,
  `create_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for ai_popupshop_agent
-- ----------------------------
CREATE TABLE `ai_popupshop_agent` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `member_miniapp_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `rebate` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for ai_popupshop_article
-- ----------------------------
CREATE TABLE `ai_popupshop_article` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `member_miniapp_id` int(11) DEFAULT NULL,
  `types` tinyint(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `content` longtext,
  `update_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for ai_popupshop_auth
-- ----------------------------
CREATE TABLE `ai_popupshop_auth` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `types` tinyint(1) DEFAULT NULL,
  `member_id` int(11) DEFAULT NULL,
  `member_miniapp_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for ai_popupshop_bank
-- ----------------------------
CREATE TABLE `ai_popupshop_bank` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `member_miniapp_id` int(11) DEFAULT NULL,
  `user_id` bigint(10) DEFAULT NULL,
  `lack_money` decimal(10,2) DEFAULT '0.00' COMMENT '锁定金额',
  `due_money` decimal(10,2) DEFAULT '0.00' COMMENT '应付款',
  `shop_money` decimal(10,2) DEFAULT '0.00' COMMENT '购物积分',
  `income_money` decimal(10,2) DEFAULT '0.00' COMMENT '累计收益',
  `update_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for ai_popupshop_bank_bill
-- ----------------------------
CREATE TABLE `ai_popupshop_bank_bill` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `member_miniapp_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT '0',
  `money` decimal(10,2) DEFAULT '0.00',
  `update_time` int(11) DEFAULT NULL,
  `message` tinytext,
  `from_uid` int(11) DEFAULT '0',
  `order_no` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='交易账单';

-- ----------------------------
-- Table structure for ai_popupshop_bank_cash
-- ----------------------------
CREATE TABLE `ai_popupshop_bank_cash` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `member_miniapp_id` int(11) DEFAULT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `money` decimal(10,2) DEFAULT '0.00' COMMENT '申请金额',
  `realmoney` decimal(10,2) DEFAULT '0.00' COMMENT '实际到账',
  `update_time` int(11) DEFAULT NULL,
  `state` tinyint(1) DEFAULT '0' COMMENT '0新申请1通过-1不通过',
  `audit_time` int(11) DEFAULT NULL,
  `trade_no` varchar(100) DEFAULT NULL,
  `msg` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='提现申请';

-- ----------------------------
-- Table structure for ai_popupshop_bank_info
-- ----------------------------
CREATE TABLE `ai_popupshop_bank_info` (
  `user_id` bigint(20) NOT NULL,
  `member_miniapp_id` bigint(20) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `idcard` varchar(20) DEFAULT NULL,
  `bankname` varchar(255) DEFAULT NULL,
  `bankid` varchar(50) DEFAULT NULL,
  `update_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='提现银行';

-- ----------------------------
-- Table structure for ai_popupshop_bank_recharge
-- ----------------------------
CREATE TABLE `ai_popupshop_bank_recharge` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `member_miniapp_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `money` decimal(10,2) DEFAULT NULL,
  `state` tinyint(1) DEFAULT '0',
  `order_no` varchar(255) DEFAULT NULL,
  `update_time` int(11) DEFAULT NULL,
  `paid_time` int(11) DEFAULT NULL,
  `paid_no` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for ai_popupshop_category
-- ----------------------------
CREATE TABLE `ai_popupshop_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `member_miniapp_id` int(11) DEFAULT NULL,
  `parent_id` int(11) DEFAULT '0',
  `root_id` int(11) DEFAULT NULL,
  `types` tinyint(1) DEFAULT '0',
  `title` varchar(50) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `picture` varchar(100) DEFAULT NULL,
  `sort` int(11) DEFAULT '0',
  `update_time` int(11) DEFAULT NULL,
  `create_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ID` (`id`) USING BTREE,
  KEY `PARENT_ID` (`parent_id`,`root_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='商品目录';

-- ----------------------------
-- Table structure for ai_popupshop_config
-- ----------------------------
CREATE TABLE `ai_popupshop_config` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `member_miniapp_id` int(11) DEFAULT '0',
  `tax` int(5) DEFAULT '0' COMMENT '手续费比例',
  `profit` int(5) DEFAULT '0' COMMENT '利润率',
  `cycle` int(5) DEFAULT '0' COMMENT '提现周期',
  `lack_cash` int(11) DEFAULT '0' COMMENT '提现限制',
  `lock_sale_day` int(11) DEFAULT '0' COMMENT '限委托(天)',
  `num_referee_people` int(11) DEFAULT '0' COMMENT '每推荐多少人可以购买一单',
  `is_wechat_touser` tinyint(1) DEFAULT '0',
  `update_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for ai_popupshop_fare
-- ----------------------------
CREATE TABLE `ai_popupshop_fare` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `member_miniapp_id` int(11) DEFAULT NULL,
  `first_weight` int(11) DEFAULT NULL,
  `first_price` float(10,2) DEFAULT NULL,
  `second_weight` int(11) DEFAULT NULL,
  `second_price` float(10,2) DEFAULT NULL,
  `update_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='运费设置';

-- ----------------------------
-- Table structure for ai_popupshop_fees
-- ----------------------------
CREATE TABLE `ai_popupshop_fees` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `member_miniapp_id` int(11) DEFAULT NULL,
  `tax` decimal(10,2) DEFAULT NULL,
  `cost` decimal(10,2) DEFAULT NULL,
  `fees` decimal(10,2) DEFAULT NULL,
  `update_time` int(11) DEFAULT NULL,
  `create_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for ai_popupshop_item
-- ----------------------------
CREATE TABLE `ai_popupshop_item` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `member_miniapp_id` int(11) DEFAULT NULL,
  `category_id` bigint(20) DEFAULT NULL,
  `category_path_id` varchar(255) DEFAULT NULL,
  `is_sale` tinyint(1) DEFAULT '0' COMMENT '0:下架、1:上架,',
  `name` varchar(255) DEFAULT NULL,
  `note` varchar(255) DEFAULT NULL,
  `sell_price` decimal(10,2) NOT NULL DEFAULT '0.00',
  `market_price` decimal(10,2) DEFAULT '0.00',
  `cost_price` decimal(10,2) DEFAULT '0.00',
  `points` int(11) DEFAULT '0',
  `repoints` int(11) DEFAULT '0',
  `weight` int(10) DEFAULT NULL,
  `imgs` text,
  `img` varchar(255) DEFAULT NULL,
  `content` mediumtext,
  `sort` int(11) DEFAULT NULL,
  `types` tinyint(1) DEFAULT NULL,
  `update_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ID` (`id`) USING BTREE,
  KEY `IS_SALE` (`is_sale`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='商品主表';

-- ----------------------------
-- Table structure for ai_popupshop_order
-- ----------------------------
CREATE TABLE `ai_popupshop_order` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `member_miniapp_id` int(11) DEFAULT NULL,
  `payment_id` varchar(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `sales_user_id` int(11) DEFAULT '0' COMMENT '重新提货来自ID',
  `order_no` varchar(50) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `real_amount` float(10,2) DEFAULT '0.00' COMMENT '单商品价格总额',
  `real_freight` float(10,2) DEFAULT '0.00' COMMENT '物流价钱',
  `order_amount` float(10,2) DEFAULT NULL,
  `order_starttime` int(11) DEFAULT NULL,
  `order_endtime` int(11) DEFAULT NULL,
  `paid_at` tinyint(1) DEFAULT '0',
  `paid_time` int(11) DEFAULT NULL,
  `paid_no` varchar(255) DEFAULT NULL,
  `express_status` tinyint(1) DEFAULT '0',
  `express_name` varchar(50) DEFAULT NULL,
  `express_phone` varchar(20) DEFAULT NULL,
  `express_address` varchar(255) DEFAULT NULL,
  `express_starttime` int(11) DEFAULT NULL,
  `express_company` varchar(100) DEFAULT NULL COMMENT '快递公司',
  `express_no` varchar(50) DEFAULT NULL COMMENT '快递单号',
  `is_del` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`,`status`),
  UNIQUE KEY `ORDER_NO` (`order_no`) USING BTREE,
  KEY `IS_DEL` (`is_del`) USING BTREE,
  KEY `STATUS` (`status`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for ai_popupshop_order_cache
-- ----------------------------
CREATE TABLE `ai_popupshop_order_cache` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) DEFAULT NULL,
  `order_no` varchar(50) DEFAULT NULL,
  `item_id` bigint(20) DEFAULT NULL,
  `buy_price` float(10,2) DEFAULT '0.00',
  `buy_nums` int(11) DEFAULT '1',
  `name` varchar(255) DEFAULT NULL,
  `img` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for ai_popupshop_regnum
-- ----------------------------
CREATE TABLE `ai_popupshop_regnum` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `member_miniapp_id` int(11) DEFAULT NULL,
  `uid` int(11) NOT NULL,
  `num` int(11) DEFAULT '0',
  `allnum` int(11) DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `UID` (`uid`) USING BTREE,
  KEY `MINAPP` (`member_miniapp_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for ai_popupshop_sales
-- ----------------------------
CREATE TABLE `ai_popupshop_sales` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `member_miniapp_id` int(11) DEFAULT NULL,
  `is_sale` tinyint(1) DEFAULT '0' COMMENT '上下架',
  `is_pay` tinyint(1) DEFAULT '0' COMMENT '是否买出去',
  `is_out` tinyint(1) DEFAULT '0' COMMENT '0正常 1强制发货',
  `store_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `house_id` int(11) DEFAULT NULL,
  `sales_user_id` int(11) DEFAULT '0',
  `user_cost_price` decimal(11,2) DEFAULT '0.00' COMMENT '成本价',
  `user_entrust_price` decimal(10,2) DEFAULT NULL,
  `user_sale_price` decimal(11,2) DEFAULT NULL COMMENT '销售价',
  `gift` text,
  `update_time` int(11) DEFAULT NULL,
  `create_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for ai_popupshop_sales_category
-- ----------------------------
CREATE TABLE `ai_popupshop_sales_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `member_miniapp_id` int(11) DEFAULT NULL,
  `title` varchar(50) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `picture` varchar(100) DEFAULT NULL,
  `sort` int(11) DEFAULT '0',
  `update_time` int(11) DEFAULT NULL,
  `create_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ID` (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='商品目录';

-- ----------------------------
-- Table structure for ai_popupshop_sales_house
-- ----------------------------
CREATE TABLE `ai_popupshop_sales_house` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `member_miniapp_id` int(11) DEFAULT NULL,
  `category_id` bigint(20) DEFAULT NULL,
  `is_sale` tinyint(1) DEFAULT '0' COMMENT '0:下架、1:删除,、2:上架,',
  `is_del` tinyint(1) DEFAULT '0',
  `title` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `note` varchar(255) DEFAULT NULL,
  `sell_price` decimal(10,2) NOT NULL DEFAULT '0.00',
  `cost_price` decimal(10,2) DEFAULT '0.00',
  `imgs` text,
  `img` varchar(255) DEFAULT NULL,
  `content` mediumtext,
  `sort` int(11) DEFAULT NULL,
  `update_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ID` (`id`) USING BTREE,
  KEY `IS_SALE` (`is_sale`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='商品主表';

-- ----------------------------
-- Table structure for ai_popupshop_sales_order
-- ----------------------------
CREATE TABLE `ai_popupshop_sales_order` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `member_miniapp_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `sale_id` int(11) DEFAULT NULL,
  `order_no` varchar(40) DEFAULT NULL,
  `status` tinyint(1) DEFAULT '0',
  `is_entrust` tinyint(1) DEFAULT '0',
  `is_del` tinyint(1) DEFAULT '0',
  `is_out` tinyint(1) DEFAULT '0' COMMENT '退货',
  `is_settle` tinyint(1) DEFAULT '0' COMMENT '是否结算了',
  `payment_id` int(11) DEFAULT NULL,
  `real_amount` float(10,2) DEFAULT '0.00' COMMENT '单商品价格总额',
  `real_freight` float(10,2) DEFAULT '0.00' COMMENT '物流价钱',
  `order_amount` float(10,2) DEFAULT '0.00',
  `order_starttime` int(11) DEFAULT NULL,
  `order_endtime` int(11) DEFAULT NULL,
  `paid_at` tinyint(1) DEFAULT '0',
  `paid_time` int(11) DEFAULT NULL,
  `paid_no` varchar(255) DEFAULT NULL,
  `express_name` varchar(50) DEFAULT NULL,
  `express_phone` varchar(20) DEFAULT NULL,
  `express_address` varchar(255) DEFAULT NULL,
  `express_status` tinyint(1) DEFAULT '0',
  `express_starttime` int(11) DEFAULT NULL,
  `express_company` varchar(100) DEFAULT NULL COMMENT '快递公司',
  `express_no` varchar(50) DEFAULT NULL COMMENT '快递单号',
  PRIMARY KEY (`id`),
  UNIQUE KEY `ORDER_NO` (`order_no`) USING BTREE,
  KEY `IS_DEL` (`is_del`) USING BTREE,
  KEY `STATUS` (`status`) USING BTREE,
  KEY `IS_ENTRUST` (`is_entrust`),
  KEY `ORDER_STARTTIME` (`order_starttime`),
  KEY `EXPRESS_STATUS` (`express_status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for ai_popupshop_sales_order_cache
-- ----------------------------
CREATE TABLE `ai_popupshop_sales_order_cache` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `is_sales` tinyint(1) DEFAULT '0' COMMENT '是否主商品',
  `is_entrust` tinyint(1) DEFAULT '0' COMMENT '是否委托',
  `is_out` tinyint(1) DEFAULT '0',
  `order_no` varchar(40) DEFAULT NULL,
  `sale_order_id` int(11) DEFAULT NULL,
  `house_id` int(20) DEFAULT NULL,
  `sale_price` decimal(10,2) DEFAULT NULL COMMENT '商品销售价',
  `name` varchar(255) DEFAULT NULL,
  `img` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ORDER_NO` (`order_no`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for ai_popupshop_sales_user
-- ----------------------------
CREATE TABLE `ai_popupshop_sales_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `member_miniapp_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `house_id` int(11) DEFAULT NULL,
  `order_no` varchar(50) DEFAULT NULL,
  `user_price` decimal(10,2) DEFAULT '0.00',
  `rebate` decimal(10,2) DEFAULT '0.00' COMMENT '成交价',
  `is_lock_rebate` tinyint(1) DEFAULT '0' COMMENT '是否结算利润',
  `is_rebate` tinyint(1) DEFAULT '0' COMMENT '是否成交',
  `is_sale` tinyint(1) DEFAULT '0' COMMENT '是否上架',
  `is_out` tinyint(1) DEFAULT '0',
  `create_time` int(11) DEFAULT NULL,
  `update_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='客户商品';

-- ----------------------------
-- Table structure for ai_popupshop_store
-- ----------------------------
CREATE TABLE `ai_popupshop_store` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `update_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
SET FOREIGN_KEY_CHECKS=1;
