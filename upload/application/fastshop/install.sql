/*
Navicat MySQL Data Transfer

Source Server         : 本地开发
Source Server Version : 50721
Source Host           : localhost:3306
Source Database       : wechat

Target Server Type    : MYSQL
Target Server Version : 50721
File Encoding         : 65001

Date: 2020-03-11 14:40:09
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for ai_fastshop_agent
-- ----------------------------
CREATE TABLE `ai_fastshop_agent` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `member_miniapp_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `rebate` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for ai_fastshop_article
-- ----------------------------
CREATE TABLE `ai_fastshop_article` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `member_miniapp_id` int(11) DEFAULT NULL,
  `types` tinyint(255) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `content` longtext,
  `update_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for ai_fastshop_auth
-- ----------------------------
CREATE TABLE `ai_fastshop_auth` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `types` tinyint(1) DEFAULT NULL,
  `member_id` int(11) DEFAULT NULL,
  `member_miniapp_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for ai_fastshop_bank
-- ----------------------------
CREATE TABLE `ai_fastshop_bank` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `member_miniapp_id` int(11) DEFAULT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `money` bigint(20) DEFAULT '0' COMMENT '帐号余额',
  `lack_money` bigint(20) DEFAULT '0' COMMENT '锁定金额',
  `due_money` bigint(20) DEFAULT '0' COMMENT '应付款',
  `shop_money` bigint(20) DEFAULT '0' COMMENT '购物积分',
  `income_money` bigint(20) DEFAULT '0' COMMENT '累计收益',
  `profit` bigint(20) DEFAULT '0',
  `update_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for ai_fastshop_bank_all
-- ----------------------------
CREATE TABLE `ai_fastshop_bank_all` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `member_miniapp_id` int(11) DEFAULT NULL,
  `uid` int(11) DEFAULT NULL,
  `account` decimal(10,2) DEFAULT NULL,
  `pyramid` decimal(10,2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for ai_fastshop_bank_cash
-- ----------------------------
CREATE TABLE `ai_fastshop_bank_cash` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `member_miniapp_id` int(11) DEFAULT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `money` bigint(20) DEFAULT '0' COMMENT '申请金额',
  `realmoney` bigint(20) DEFAULT '0' COMMENT '实际到账',
  `update_time` int(11) DEFAULT NULL,
  `state` tinyint(1) DEFAULT '0' COMMENT '0新申请1通过-1不通过',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='提现申请列表';

-- ----------------------------
-- Table structure for ai_fastshop_bank_info
-- ----------------------------
CREATE TABLE `ai_fastshop_bank_info` (
  `user_id` bigint(20) NOT NULL,
  `member_miniapp_id` bigint(20) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `idcard` varchar(20) DEFAULT NULL,
  `bankname` varchar(255) DEFAULT NULL,
  `bankid` varchar(50) DEFAULT NULL,
  `update_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='提现银行';

-- ----------------------------
-- Table structure for ai_fastshop_bank_logs
-- ----------------------------
CREATE TABLE `ai_fastshop_bank_logs` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `member_miniapp_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT '0',
  `money` bigint(20) DEFAULT '0',
  `update_time` int(11) DEFAULT NULL,
  `message` tinytext,
  `from_uid` int(11) DEFAULT '0',
  `order_no` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id` (`id`),
  KEY `id_2` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='财务变动记录';

-- ----------------------------
-- Table structure for ai_fastshop_bank_recharge
-- ----------------------------
CREATE TABLE `ai_fastshop_bank_recharge` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `member_miniapp_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `money` bigint(20) DEFAULT NULL,
  `state` tinyint(1) DEFAULT '0',
  `order_no` varchar(255) DEFAULT NULL,
  `update_time` int(11) DEFAULT NULL,
  `paid_time` int(11) DEFAULT NULL,
  `paid_no` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for ai_fastshop_banner
-- ----------------------------
CREATE TABLE `ai_fastshop_banner` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `member_miniapp_id` int(11) DEFAULT '0',
  `group_id` tinyint(1) DEFAULT NULL,
  `open_type` varchar(100) DEFAULT NULL,
  `title` varchar(50) DEFAULT NULL,
  `link` varchar(200) DEFAULT NULL,
  `picture` varchar(255) DEFAULT NULL,
  `sort` int(11) DEFAULT '0',
  `update_time` int(11) DEFAULT NULL,
  `create_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for ai_fastshop_cate
-- ----------------------------
CREATE TABLE `ai_fastshop_cate` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `member_miniapp_id` int(11) DEFAULT NULL,
  `parent_id` int(11) DEFAULT '0',
  `root_id` int(11) DEFAULT NULL,
  `types` tinyint(1) DEFAULT '0',
  `title` varchar(50) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `picture` varchar(100) DEFAULT NULL,
  `sort` int(11) DEFAULT '0',
  `update_time` int(11) DEFAULT NULL,
  `create_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ID` (`id`) USING BTREE,
  KEY `PARENT_ID` (`parent_id`,`root_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='商品目录';

-- ----------------------------
-- Table structure for ai_fastshop_config
-- ----------------------------
CREATE TABLE `ai_fastshop_config` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `member_miniapp_id` int(11) DEFAULT NULL,
  `is_pay_types` tinyint(4) DEFAULT '0',
  `goodpay_tax` varchar(10) DEFAULT NULL,
  `shop_types` tinyint(1) DEFAULT '0' COMMENT '购买限制',
  `regvip_price` int(10) DEFAULT '0' COMMENT '开通会员费用',
  `regvip_level1_ratio` int(2) DEFAULT '0' COMMENT '一级返佣',
  `regvip_level2_ratio` int(5) DEFAULT '0' COMMENT '二级返佣',
  `reward_types` int(10) DEFAULT '0' COMMENT '奖励方式',
  `reward_nth` int(10) DEFAULT '0' COMMENT '推荐奖励/奖励倍数',
  `reward_ratio` int(10) DEFAULT '0' COMMENT '间推荐/绩效奖励比例',
  `tax` int(5) DEFAULT '0' COMMENT '手续费比例',
  `profit` int(5) DEFAULT '0' COMMENT '利润率',
  `shopping_name` varchar(255) DEFAULT NULL,
  `shopping` int(5) DEFAULT NULL COMMENT '购物金比例',
  `cycle` int(5) DEFAULT NULL COMMENT '提现周期',
  `message` varchar(255) DEFAULT NULL,
  `payment_type` tinyint(1) DEFAULT '0' COMMENT '0关闭1应付2购物3',
  `payment_point` tinyint(4) DEFAULT NULL,
  `payment_type_shop` tinyint(1) DEFAULT NULL,
  `payment_point_shop` tinyint(4) DEFAULT NULL,
  `amountlimit` int(11) DEFAULT NULL,
  `lack_cash` int(11) DEFAULT '0' COMMENT '提现限制',
  `is_priority` tinyint(1) DEFAULT '0',
  `update_time` int(11) DEFAULT NULL,
  `day_ordernum` tinyint(4) DEFAULT '0' COMMENT '用户限抢',
  `sale_ordernum` tinyint(4) DEFAULT '0' COMMENT '活动抢购',
  `old_users` tinyint(4) DEFAULT '0' COMMENT '是否老用户',
  `rules` varchar(255) DEFAULT NULL,
  `platform_ratio` tinyint(4) DEFAULT '0' COMMENT '平台奖励比例()',
  `platform_amout` int(11) DEFAULT '0' COMMENT '平台奖励条件',
  `lock_sale_day` int(11) DEFAULT '0' COMMENT '限委托(天)',
  `num_referee_people` int(11) DEFAULT '0' COMMENT '每推荐多少人可以购买一单',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for ai_fastshop_entrust
-- ----------------------------
CREATE TABLE `ai_fastshop_entrust` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `member_miniapp_id` int(11) DEFAULT NULL,
  `item_id` int(11) DEFAULT NULL,
  `gite_count` int(11) DEFAULT NULL,
  `entrust_price` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for ai_fastshop_entrust_list
-- ----------------------------
CREATE TABLE `ai_fastshop_entrust_list` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `member_miniapp_id` int(11) DEFAULT NULL,
  `order_no` varchar(50) DEFAULT NULL,
  `entrust_id` int(11) DEFAULT NULL,
  `item_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `order_amount` int(11) DEFAULT NULL,
  `entrust_price` int(11) DEFAULT NULL,
  `rebate` int(11) DEFAULT '0' COMMENT '返利多少钱(分)',
  `is_rebate` tinyint(1) DEFAULT '0' COMMENT '是否返利',
  `create_time` int(11) DEFAULT NULL,
  `update_time` int(11) DEFAULT NULL,
  `is_diy` tinyint(1) DEFAULT '0',
  `is_under` tinyint(1) DEFAULT '0' COMMENT '是否下架',
  `is_fusion` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for ai_fastshop_fare
-- ----------------------------
CREATE TABLE `ai_fastshop_fare` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `member_miniapp_id` int(11) DEFAULT NULL,
  `first_weight` int(11) DEFAULT NULL,
  `first_price` float(10,2) DEFAULT NULL,
  `second_weight` int(11) DEFAULT NULL,
  `second_price` float(10,2) DEFAULT NULL,
  `update_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='运费设置';

-- ----------------------------
-- Table structure for ai_fastshop_group
-- ----------------------------
CREATE TABLE `ai_fastshop_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `member_miniapp_id` int(11) DEFAULT NULL,
  `item_id` bigint(20) DEFAULT NULL,
  `amount` decimal(10,2) DEFAULT NULL,
  `hao_people` int(10) DEFAULT NULL,
  `uids` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='团购数据';

-- ----------------------------
-- Table structure for ai_fastshop_item
-- ----------------------------
CREATE TABLE `ai_fastshop_item` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `member_miniapp_id` int(11) DEFAULT NULL,
  `category_id` bigint(20) DEFAULT NULL,
  `category_path_id` varchar(255) DEFAULT NULL,
  `is_shopping` tinyint(1) DEFAULT '0',
  `is_sale` tinyint(1) DEFAULT '0' COMMENT '0:下架、1:删除,、2:上架,',
  `name` varchar(255) DEFAULT NULL,
  `price` int(11) DEFAULT '0',
  `sell_price` decimal(10,2) NOT NULL DEFAULT '0.00',
  `market_price` decimal(10,2) DEFAULT '0.00',
  `cost_price` decimal(10,2) DEFAULT '0.00',
  `points` int(11) DEFAULT '0',
  `repoints` int(11) DEFAULT '0',
  `weight` int(10) DEFAULT NULL,
  `imgs` text,
  `img` varchar(255) DEFAULT NULL,
  `content` mediumtext,
  `sort` int(11) DEFAULT NULL,
  `types` tinyint(1) DEFAULT NULL,
  `update_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ID` (`id`) USING BTREE,
  KEY `IS_SALE` (`is_sale`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='商品主表';

-- ----------------------------
-- Table structure for ai_fastshop_order
-- ----------------------------
CREATE TABLE `ai_fastshop_order` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `member_miniapp_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `sale_id` int(11) DEFAULT NULL,
  `order_no` varchar(40) DEFAULT NULL,
  `status` tinyint(1) DEFAULT '0',
  `is_fusion` tinyint(1) DEFAULT '0',
  `is_entrust` tinyint(1) DEFAULT '0',
  `is_del` tinyint(1) DEFAULT '0',
  `is_point` tinyint(1) DEFAULT '0',
  `payment_id` int(11) DEFAULT NULL,
  `real_amount` decimal(10,2) DEFAULT '0.00' COMMENT '单商品价格总额',
  `real_freight` decimal(10,2) DEFAULT '0.00' COMMENT '物流价钱',
  `order_amount` decimal(10,2) DEFAULT '0.00',
  `order_starttime` int(11) DEFAULT NULL,
  `order_endtime` int(11) DEFAULT NULL,
  `paid_at` tinyint(1) DEFAULT '0',
  `paid_time` int(11) DEFAULT NULL,
  `paid_no` varchar(255) DEFAULT NULL,
  `express_name` varchar(50) DEFAULT NULL,
  `express_phone` varchar(20) DEFAULT NULL,
  `express_address` varchar(255) DEFAULT NULL,
  `express_status` tinyint(1) DEFAULT '0',
  `express_starttime` int(11) DEFAULT NULL,
  `express_company` varchar(100) DEFAULT NULL COMMENT '快递公司',
  `express_no` varchar(50) DEFAULT NULL COMMENT '快递单号',
  `message` varchar(255) DEFAULT NULL COMMENT '留言',
  PRIMARY KEY (`id`),
  UNIQUE KEY `ORDER_NO` (`order_no`) USING BTREE,
  KEY `IS_DEL` (`is_del`) USING BTREE,
  KEY `STATUS` (`status`) USING BTREE,
  KEY `IS_ENTRUST` (`is_entrust`),
  KEY `ORDER_STARTTIME` (`order_starttime`),
  KEY `EXPRESS_STATUS` (`express_status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for ai_fastshop_order_cache
-- ----------------------------
CREATE TABLE `ai_fastshop_order_cache` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `order_no` varchar(40) DEFAULT NULL,
  `item_id` bigint(20) DEFAULT NULL,
  `sale_price` bigint(20) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `img` varchar(255) DEFAULT NULL,
  `gift` text,
  `entrust` varchar(255) DEFAULT NULL COMMENT '真是委托假是提货',
  `fusion_state` tinyint(1) DEFAULT '0' COMMENT '聚变产品状态 0不委托1委托',
  PRIMARY KEY (`id`),
  KEY `ORDER_NO` (`order_no`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for ai_fastshop_regnum
-- ----------------------------
CREATE TABLE `ai_fastshop_regnum` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `member_miniapp_id` int(11) DEFAULT NULL,
  `uid` int(11) NOT NULL,
  `num` int(11) DEFAULT '0',
  `allnum` int(11) DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `UID` (`uid`) USING BTREE,
  KEY `MINAPP` (`member_miniapp_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for ai_fastshop_sales
-- ----------------------------
CREATE TABLE `ai_fastshop_sales` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `member_miniapp_id` int(11) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `class_id` int(11) DEFAULT NULL,
  `is_fusion` tinyint(1) DEFAULT '0' COMMENT '聚变还是裂变',
  `is_vip` tinyint(1) DEFAULT NULL,
  `is_newuser` tinyint(1) DEFAULT NULL,
  `types` tinyint(1) DEFAULT '0',
  `title` varchar(255) DEFAULT NULL,
  `item_id` int(11) DEFAULT NULL,
  `sale_nums` int(10) DEFAULT NULL,
  `cost_price` int(11) DEFAULT NULL,
  `sale_price` int(11) DEFAULT NULL,
  `market_price` int(11) DEFAULT NULL,
  `gift` text,
  `img` varchar(255) DEFAULT NULL,
  `sort` int(11) DEFAULT NULL,
  `start_time` int(11) DEFAULT NULL,
  `end_time` int(11) DEFAULT NULL,
  `update_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `NEWUSER` (`is_newuser`),
  KEY `VIP` (`is_vip`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for ai_fastshop_shopping
-- ----------------------------
CREATE TABLE `ai_fastshop_shopping` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `member_miniapp_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `order_no` varchar(50) DEFAULT NULL,
  `status` tinyint(1) DEFAULT '0',
  `is_del` tinyint(1) DEFAULT '0',
  `payment_id` int(11) DEFAULT NULL,
  `real_amount` float(10,2) DEFAULT '0.00' COMMENT '单商品价格总额',
  `real_freight` float(10,2) DEFAULT '0.00' COMMENT '物流价钱',
  `order_amount` float(10,2) DEFAULT NULL,
  `order_starttime` int(11) DEFAULT NULL,
  `order_endtime` int(11) DEFAULT NULL,
  `paid_at` tinyint(1) DEFAULT '0',
  `paid_time` int(11) DEFAULT NULL,
  `paid_no` varchar(255) DEFAULT NULL,
  `express_name` varchar(50) DEFAULT NULL,
  `express_phone` varchar(20) DEFAULT NULL,
  `express_address` varchar(255) DEFAULT NULL,
  `express_status` tinyint(1) DEFAULT '0',
  `express_starttime` int(11) DEFAULT NULL,
  `express_company` varchar(100) DEFAULT NULL COMMENT '快递公司',
  `express_no` varchar(50) DEFAULT NULL COMMENT '快递单号',
  PRIMARY KEY (`id`),
  UNIQUE KEY `ORDER_NO` (`order_no`) USING BTREE,
  KEY `IS_DEL` (`is_del`) USING BTREE,
  KEY `STATUS` (`status`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for ai_fastshop_shopping_cache
-- ----------------------------
CREATE TABLE `ai_fastshop_shopping_cache` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `order_no` varchar(50) DEFAULT NULL,
  `item_id` bigint(20) DEFAULT NULL,
  `buy_price` float(10,2) DEFAULT '0.00',
  `buy_nums` int(11) DEFAULT '1',
  `name` varchar(255) DEFAULT NULL,
  `img` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for ai_fastshop_store
-- ----------------------------
CREATE TABLE `ai_fastshop_store` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `update_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for ai_fastshop_times
-- ----------------------------
CREATE TABLE `ai_fastshop_times` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `member_miniapp_id` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `start_time` int(4) DEFAULT NULL,
  `end_time` int(4) DEFAULT NULL,
  `sort` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for ai_fastshop_vip
-- ----------------------------
CREATE TABLE `ai_fastshop_vip` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `member_miniapp_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `state` tinyint(1) DEFAULT '0',
  `order_no` varchar(255) DEFAULT NULL,
  `update_time` int(11) DEFAULT NULL,
  `paid_time` int(11) DEFAULT NULL,
  `paid_no` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
SET FOREIGN_KEY_CHECKS=1;
