<?php
/**
 * 接口签名
 */
namespace sign;

class WxException extends \Exception
{
    public static $errorMessage = [
        '20002' => '系统错误，请联系客服人员后重试',    // 加密敏感信息失败
        '30000' => 'xml数据异常！',
        '30001' => '数组数据异常！',
    ];
    public static $defaultMessage = '未知错误';

    public function __construct($code = 0, $message = ""){
        if ($message === "") {
            $message  = static::$errorMessage[$code] ?? static::$defaultMessage;
        }
        parent::__construct($message, $code);
    }

    public function __toString(){
        return json_encode($this->getResponse());
    }

    public function getResponse(){
        return $this->response;
    }
}